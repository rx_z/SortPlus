#ifndef SORTTHREAD
#define SORTTHREAD

#include <QThread>
#include <ctime>

typedef int KeyType;
typedef int DataType;

typedef struct
{
    KeyType key;
}RecordNode;

typedef struct
{
    int n;
    RecordNode record[1000];
}SortObject;


class SortThread : public QThread
{
    Q_OBJECT
public:
    /*传值数组，数量，冷冻时间，排序种类*/
    SortThread(int *,int, int, int);

signals:
    /*信号*更新数组，排序中扫描值1，排序中扫描值2*/
    void updatea(int *,int,int);

    /*信号*排序趟数，交换次数，比较次数*/
    void updatet(int,int,int);

    /*信号*程序用时*/
    void updateTime(double);

protected:
    /*排序中扫描值1，排序中扫描值2*/
    void senda(int,int);

    /*排序趟数，交换次数，比较次数*/
    void sendt(int,int,int);

    /*开始时间，结束时间*/
    void sendTime(clock_t,clock_t);

    /*线程run方法*/
    void run();

private:
    int threadFrd;//线程休眠时间

    int bubbleArray[1000];//冒泡测试数组，已废弃

    int sendArray[1000];//传递数组

    int size;//参排数值大小

    int index;//排序选项下拉菜单索引值

    int catched1;//排序中扫描值1

    int catched2;//排序中扫描值2

    int sortTle;//排序趟数

    int sortMov;//移动次数

    int sortCmp;//比较次数

    clock_t staTime;//函数开始时间标记

    clock_t endTime;//函数结束时间标记

    void bubbleSort();//冒泡排序

    void quickSort(SortObject *pvector);//快排函数
    void quick(SortObject *pvector, int l, int r);//快排算法

    void heapSort(SortObject *pvector);//堆排序算法
    void sift(SortObject *pvector, int size, int p);//调整堆函数

    void merge(RecordNode r[], RecordNode r1[], int low, int m, int high);//两组归并
    void mergePass(RecordNode r[], RecordNode r1[], int n, int length);//一趟归并
    void mergeSort(SortObject *pvector);//二路归并

    void radixSort(int arr[], int begin, int end, int d);//基数排序函数

    void insertSort(SortObject *pvector);//直接插入排序函数

    void binInsertSort(SortObject *pvector);//二分插入排序函数

    void shellSort(SortObject *pvector,int d);//希尔排序函数

    void selectSort(SortObject *pvector);//直接选择排序函数
};

#endif // SORTTHREAD

