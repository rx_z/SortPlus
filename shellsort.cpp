#include "randnum.h"
#include <windows.h>
#include <sortthread.h>

void SortThread::shellSort(SortObject * pvector, int d)
{	/* 按递增序进行Shell排序 */
    int i, j, inc;
    sortTle = 0;
    sortMov = 0;
    sortCmp = 0;
    pvector->n = this->size;
    RecordNode temp, *data = pvector->record;
    for (inc = d; inc > 0; inc /= 2)
    {
        sortTle++;
        this->sendt(sortTle,sortMov,sortCmp);
        /* inc 为本趟shell排序增量 */
        for (i = inc; i < pvector->n; i++)
        {
            sortCmp++;
            this->sendt(sortTle,sortMov,sortCmp);
            /* 保存待插入记录Ri*/
            temp = data[i];
            for (j = i - inc; j >= 0 && temp.key < data[j].key; j -= inc)
            {
                sortCmp++;
                this->sendt(sortTle,sortMov,sortCmp);
                /* 查找插入位置，记录后移 */
                data[j + inc] = data[j];
                this->sendArray[j] = data[j].key;
            }
            this->senda(j+inc,j);
            /* 插入记录Ri */
            data[j + inc] = temp;
            sortMov++;
            for(int ii = 0; ii < pvector->n; ii++)
            {
                this->sendArray[ii] = data[ii].key;
            }
            this->sendt(sortTle,sortMov,sortCmp);
            this->senda(i,-1);
        }
        for(int ii = 0; ii < pvector->n; ii++)
        {
            this->sendArray[ii] = data[ii].key;
        }
    }
    for(int i = 0; i < size; i++)
    {
        senda(i,-10);
    }
    this->sendt(sortTle,sortMov,sortCmp);
}
