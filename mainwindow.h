void on_pushButton_3_clicked();
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <sortwidget.h>
#include <ctime>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_2_clicked();

    /*绘图线程槽*/
    void sort_thread(int *,int,int);

    /*更改title线程槽*/
    void sort_title(int,int,int);

    /*设置时间线程槽*/
    void sort_time(double);

    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::MainWindow *ui;

    sortwidget *widget;

    int sortTemp[1000];//传递值

    int sortNum;//参排总数

    int sortFrdTime;//排序演示冷冻时间
};

#endif // MAINWINDOW_H
