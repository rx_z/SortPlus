#ifndef RANDNUM
#define RANDNUM

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtCore/QCoreApplication>
#include<iostream>
#include<cstdlib>
#include<cmath>
#include<ctime>
#include<fstream>
#include<QMessageBox>
using namespace std;

int possionRand(double lambda);//生泊松分布成随机数的主程序
double pRand();//产生普通确定范围的随机数函数

int gaussianRand(double variance, double mean);//高斯分布函数+

int orderedNum();//有序数列

#endif // RANDNUM

