#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "sortthread.h"
#include "sortwidget.h"
#include <ctime>
#include "randnum.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    widget = new sortwidget(this);
    QVBoxLayout * layout = new QVBoxLayout;
    layout->addWidget(widget);
    ui->widget->setLayout(layout);

    this->sortNum = 10;
}

MainWindow::~MainWindow()
{
    delete ui;
}

/*排序线程槽*/
void MainWindow::sort_thread(int array[],int cat1,int cat2)
{
    for(int i = 0;i < this->sortNum; i++)
    {
        widget->array[i] = array[i];
    }
    widget->c1 = cat1;
    widget->c2 = cat2;
    widget->repaint();
}

/*开始排序事件*/
void MainWindow::on_pushButton_2_clicked()
{
    if(ui->label_6->text() == "0")
    {
        QMessageBox::information(NULL,"提示","请生成参加排序的随机数");
        return;
    }
    switch(ui->comboBox->currentIndex())
    {
    /*冒泡*/
    case 0:
    {
        ui->label_4->setText("冒泡排序");
        ui->label_9->setText("交换次数");
    }
        break;
        /*快排*/
    case 1:
    {
        ui->label_4->setText("快速排序");
        ui->label_9->setText("交换次数");
    }
        break;
        /*直接插入*/
    case 2:
    {
        ui->label_4->setText("直接插入排序");
        ui->label_9->setText("移动次数");
    }
        break;
        /*二分插入*/
    case 3:
    {
        ui->label_4->setText("二分插入排序");
        ui->label_9->setText("移动次数");
    }
        break;
        /*希尔排序*/
    case 4:
    {
        ui->label_4->setText("希尔排序");
        ui->label_9->setText("移动次数");
    }
        break;
        /*直接选择*/
    case 5:
    {
        ui->label_4->setText("直接选择排序");
        ui->label_9->setText("交换次数");
    }
        break;
        /*堆排序*/
    case 6:
    {
        ui->label_4->setText("堆排序");
        ui->label_9->setText("交换次数");
    }
        break;
        /*基数*/
    case 7:
    {
        ui->label_4->setText("基数排序");
        ui->label_9->setText("交换次数");
    }
        break;
        /*归并*/
    case 8:
    {
        ui->label_4->setText("归并排序");
        ui->label_9->setText("交换次数");
    }
        break;
    default:
        break;
    }
    this->sortFrdTime = 3000 + ui->horizontalSlider->value();
    SortThread *pst = new SortThread(sortTemp, sortNum, this->sortFrdTime, ui->comboBox->currentIndex());
    pst->start();
    connect(pst,&SortThread::updatea,this,&MainWindow::sort_thread);
    connect(pst,&SortThread::updatet,this,&MainWindow::sort_title);
    connect(pst,&SortThread::updateTime,this,&MainWindow::sort_time);
}

void MainWindow::sort_title(int sortTle, int sortMov, int sortCmp)
{
    QString s,s1,s2;
    s = QString::number(sortTle);
    s1 = QString::number(sortMov);
    s2 = QString::number(sortCmp);
    ui->label_8->setText(s);
    ui->label_10->setText(s1);
    ui->label_12->setText(s2);
}

void MainWindow::sort_time(double t)
{
    QString s;
    s = QString("%1").arg(t);
    s += "秒";
    ui->label_14->setText(s);
}

/*生成随机数*/
void MainWindow::on_pushButton_clicked()
{
    QString s;
    this->sortNum = ui->spinBox->value();
    this->widget->size = this->sortNum;
    s = QString("%1").arg(this->sortNum);
    ui->label_6->setText(s);
    srand((unsigned)time(NULL));
    switch(ui->comboBox_2->currentIndex())
    {
    /*泊松分布*/
    case 0:
    {
        ui->label_3->setText("泊松分布");
        for(int i = 0; i < this->sortNum; i++)
        {
            this->sortTemp[i] = possionRand(380);
            this->widget->array[i] = this->sortTemp[i];
        }
    }
        break;
    /*这是一个有一点小问题的高斯分布*/
    case 1:
    {
        ui->label_3->setText("高斯分布");
        for(int i = 0; i < this->sortNum; i++)
        {
            this->sortTemp[i] = gaussianRand(50,220);
            this->widget->array[i] = this->sortTemp[i];
        }
    }
        break;
    case 2:
    {
        ui->label_3->setText("快排克星");
        for(int i = 1; i <= this->sortNum; i++)
        {
            this->sortTemp[sortNum-i] = i;
            this->widget->array[sortNum-i] = this->sortTemp[sortNum-i];
        }
    }
        break;
    default:
        break;
    }
    sort_thread(widget->array,-1,-1);
    widget->repaint();
}

/*文件读取*/
void MainWindow::on_pushButton_3_clicked()
{
    QString s;
    QMessageBox::information(NULL,"提示","在e盘创建一个mysort.txt文件,数值使用回车隔开");
    ifstream infile;
    infile.open("e://mysort.txt");
    if(!infile)
    {
        QMessageBox::information(NULL,"提示","文件读取错误请重试");
    }
    else
    {
        QMessageBox::information(NULL,"提示","文件读取成功");
        int i = 0;
        while(infile >> this->sortTemp[i++]);
        this->sortNum = i - 1;
        infile.close();
        this->widget->size = this->sortNum;
        s = QString::number(this->sortNum);
        ui->label_6->setText(s);
        ui->label_3->setText("用户文件");
        for(int i = 0; i < this->sortNum; i++)
        {
            this->widget->array[i] = this->sortTemp[i];
        }
    }
}
