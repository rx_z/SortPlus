#include "sortthread.h"
#include <QMessageBox>

SortObject vector1, vector2, a;
int radixSend[1000];

SortThread::SortThread(int array[], int size, int TFrd, int index)
{
    //copy
    for(int i = 0; i < 1000; i++)
    {
        this->sendArray[i] = array[i];
        vector1.record[i].key = array[i];
    }
    this->size = size;
    this->threadFrd = TFrd;
    this->index = index;
}

/*线程run方法*/
void SortThread::run()
{
    switch(this->index)
    {
    /*冒泡*/
    case 0:
    {
        staTime = clock();
        bubbleSort();
        endTime = clock();
        this->sendTime(staTime,endTime);
    }
        break;
    /*快排*/
    case 1:
    {
        staTime = clock();
        quickSort(&vector1);
        endTime = clock();
        this->sendTime(staTime,endTime);
    }
        break;
        /*直接插入*/
    case 2:
    {
        staTime = clock();
        insertSort(&vector1);
        endTime = clock();
        this->sendTime(staTime,endTime);
    }
        break;
        /*二分插入*/
    case 3:
    {
        staTime = clock();
        binInsertSort(&vector1);
        endTime = clock();
        this->sendTime(staTime,endTime);
    }
        break;
        /*希尔排序*/
    case 4:
    {
        staTime = clock();
        shellSort(&vector1,4);
        endTime = clock();
        this->sendTime(staTime,endTime);
    }
        break;
        /*直接选择*/
    case 5:
    {
        staTime = clock();
        selectSort(&vector1);
        endTime = clock();
        this->sendTime(staTime,endTime);
    }
        break;
        /*堆排序*/
    case 6:
    {
        staTime = clock();
        heapSort(&vector1);
        endTime = clock();
        this->sendTime(staTime,endTime);
    }
        break;
        /*基数*/
    case 7:
    {
        for(int i = 0; i < this->size; i++)
        {
            radixSend[i] = vector1.record[i].key;
        }
        staTime = clock();
        radixSort(radixSend,0,this->size,3);
        endTime = clock();
        this->sendTime(staTime,endTime);
    }
        break;
        /*归并*/
    case 8:
    {
        staTime = clock();
        mergeSort(&vector1);
        endTime = clock();
        this->sendTime(staTime,endTime);
    }
        break;
    default:
        break;
    }
    sleep(2);
}

/*发送数组信号函数*/
void SortThread::senda(int catched1,int catched2)
{
    int tempa[1000];
    int m,n;
    for(int i = 0; i < 1000; i++)
    {
        tempa[i] = this->sendArray[i];
        m = catched1;
        n = catched2;
    }
    emit updatea(tempa,m,n);
    this->usleep(this->threadFrd);
}

/*发送排序数值信号函数*/
void SortThread::sendt(int sortTle, int sortMov, int sortCmp)
{
    emit updatet(sortTle,sortMov,sortCmp);
}

/*发送函数用时函数*/
void SortThread::sendTime(clock_t staTime, clock_t endTime)
{
    double t = (double)(endTime - staTime) / CLOCKS_PER_SEC;
    emit updateTime(t);
}

/*冒泡排序--测试用*/
void SortThread::bubbleSort()
{
    sortTle = 0;
    sortMov = 0;
    sortCmp = 0;
    int temp;
    for(int i = 0;i < this->size; i++)
    {
        sortTle++;
        //sortCmp++;
        sendt(sortTle,sortMov,sortCmp);
        for(int j = this->size-1; j >= i; j--)
        {
            sortCmp++;
            sendt(sortTle,sortMov,sortCmp);
            if(this->sendArray[j] < this->sendArray[j-1])
            {
                sortMov++;
                senda(j,j-1);
                sendt(sortTle,sortMov,sortCmp);
                temp = this->sendArray[j-1];
                this->sendArray[j-1] = this->sendArray[j];
                this->sendArray[j] = temp;
            }
        }
    }
    for(int i = 0; i < size; i++)
    {
        senda(i,-10);
    }
    sendt(sortTle,sortMov,sortCmp);
}
