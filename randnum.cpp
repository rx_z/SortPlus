#include "randnum.h"

/*泊松分布随机函数*/
int possionRand(double lambda)
{
    int k = 0;
    long double p = 1.0;
    long double l = exp(-1.0*lambda);
    while(p >= l)
    {
        double u = pRand();
        p *= u;
        k++;
    }
    return k-1;
}

/*随机数生成函数*/
double pRand()
{
    double f;
    f = (float)(rand()%100);
    return f/100;
}

/*高斯分布随机函数*/
int gaussianRand(double variance, double mean)
{
    double temp;
    double u1 = (double)rand() / RAND_MAX;
    double u2 = (double)rand() / RAND_MAX;
    temp = sqrt(-2 * (log(u1)))*sin(2 * 3.1415926*(u2));
    temp = variance*temp + mean;
    if(temp < 0)
    {
        temp *= -1;
    }
    return temp;
}

///*有序数列*/
//int orderedNum()
//{
//    for(int i = 1; i < 1000; i++)
//    {
//        return i;
//    }
//}
