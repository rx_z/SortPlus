#include "randnum.h"
#include <windows.h>
#include <sortthread.h>

void SortThread::selectSort(SortObject * pvector)
{
    /* 按递增序进行直接选择排序 */
    int i, j, k;
    sortTle = 0;
    sortMov = 0;
    sortCmp = 0;
    RecordNode temp, *data = pvector->record;
    pvector->n = this->size;
    for (i = 0; i < pvector->n - 1; i++)
    {
        sortTle++;
        this->sendt(sortTle,sortMov,sortCmp);
        /* 做n-1趟选择排序 */
        k = i;
        this->sendArray[i] = data[i].key;
        this->senda(i,-1);
        /* 在无序区内找出排序码最小的记录Rk*/
        for (j = i + 1; j < pvector->n; j++)
        {
            sortCmp++;
            this->sendt(sortTle,sortMov,sortCmp);
            if (data[j].key < data[k].key)
            {
                sortMov++;
                this->sendt(sortTle,sortMov,sortCmp);
                k = j;
                this->sendArray[j] = data[j].key;
                this->senda(j,-1);
            }
            this->sendArray[j] = data[j].key;
            this->senda(j,-1);
        }
        if (k != i)
        {
            /* 记录Rk与Ri互换 */
            sortCmp++;
            this->sendt(sortTle,sortMov,sortCmp);
            temp = data[i];
            data[i] = data[k];
            data[k] = temp;
            this->sendArray[i] = data[i].key;
            this->senda(i,-1);
        }
    }
    for(int i = 0; i < size; i++)
    {
        senda(i,-10);
    }
    this->sendt(sortTle,sortMov,sortCmp);
}
