#-------------------------------------------------
#
# Project created by QtCreator 2015-10-12T13:24:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = sortplus
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    sortwidget.cpp \
    sortthread.cpp \
    randnum.cpp \
    quicksort.cpp \
    heapsort.cpp \
    mergesort.cpp \
    radixsort.cpp \
    insertsort.cpp \
    bininsertsort.cpp \
    shellsort.cpp \
    selectsort.cpp

HEADERS  += mainwindow.h \
    sortthread.h \
    sortwidget.h \
    randnum.h

FORMS    += mainwindow.ui
